package com.turing.util;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @author yuwen
 * @date 2018/11/13
 */
public class URLUtil {

    public static String handleQQMusicUrl(String url) {
        int lastIndex = StringUtils.lastIndexOf(url, "=");
        try {
            url = StringUtils.substring(url, 0, lastIndex + 1)
                    + URLEncoder.encode(StringUtils.substring(url, lastIndex + 1), StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            return null;
        }
        return url;
    }
}
