package com.turing.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author yuwen
 * @date 2018/11/13
 */
public class JsonUtil {

    public static String handleQQMusicJson(String json) {
        int firstIndex = StringUtils.indexOf(json, '(') + 1;
        int lastIndex = StringUtils.lastIndexOf(json, ')');
        return StringUtils.substring(json, firstIndex, lastIndex);
    }
}
