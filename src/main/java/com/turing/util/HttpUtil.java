package com.turing.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Map;

/**
 * @author yuwen
 * @date 2018/11/12
 */
public class HttpUtil {

    // 连接超时毫秒数
    private static final int CONNECT_TIMEOUT = 5000;
    // 传输超时毫秒数
    private static final int SOCKET_TIMEOUT = 10000;
    // 获取请求超时毫秒数
    private static final int REQUEST_CONNECT_TIMEOUT = 3000;
    // 最大连接数
    private static final int MAX_CONNECT = 200;
    // 每个路由基础连接数
    private static final int CONNECT_ROUTE = 20;
    // 连接有效时间
    private static final int VALIDATE_TIME = 30000;

    private static PoolingHttpClientConnectionManager manager = null;
    private static CloseableHttpClient client = null;

    static {
        PlainConnectionSocketFactory socketFactory = PlainConnectionSocketFactory.getSocketFactory();
        LayeredConnectionSocketFactory sslConnSocketFactory = createSSLConnSocketFactory();
        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", socketFactory)
                .register("https", sslConnSocketFactory)
                .build();
        manager = new PoolingHttpClientConnectionManager(registry);
        // 最大连接数
        manager.setMaxTotal(MAX_CONNECT);
        // 设置每个路由基础连接数量
        manager.setDefaultMaxPerRoute(CONNECT_ROUTE);
        // 设置可用空闲连接的过期时间
        manager.setValidateAfterInactivity(VALIDATE_TIME);
        SocketConfig socketConfig = SocketConfig.custom().setSoTimeout(CONNECT_TIMEOUT).build();
        manager.setDefaultSocketConfig(socketConfig);
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setSocketTimeout(SOCKET_TIMEOUT)
                .build();
        client = HttpClients.custom()
                .setConnectionManager(manager)
                .setDefaultRequestConfig(requestConfig)
                .setRetryHandler((exception, executionCount, context) -> {
                    if (executionCount >= 3) {
                        // 已经错误尝试超过3次
                        return false;
                    }
                    if (exception instanceof SSLHandshakeException
                            || exception instanceof ConnectTimeoutException
                            || exception instanceof UnknownHostException
                            || exception instanceof SSLException) {
                        // ssl证书不存在 或不被信任, 未知主机, 其他ssl异常
                        return false;
                    }
                    if (exception instanceof NoHttpResponseException
                            || exception instanceof InterruptedIOException) {
                        // 如果没有http响应(服务器挂掉)， IO传输中断
                        return true;
                    }
                    HttpClientContext clientContext = HttpClientContext.adapt(context);
                    HttpRequest request = clientContext.getRequest();
                    if (!(request instanceof HttpEntityEnclosingRequest)) {
                        // 如果请求视为幂等 则重试
                        return true;
                    }
                    return false;
                }).build();
        if (manager != null && manager.getTotalStats() != null) {
            System.out.println("客户池状态: " + manager.getTotalStats().toString());
        }
    }

    /**
     * 创建 ssl 连接工厂
     *
     * @return 工厂
     */
    private static SSLConnectionSocketFactory createSSLConnSocketFactory() {
        SSLConnectionSocketFactory factory = null;
        SSLContext context;

        try {
            context = SSLContext.getInstance(SSLConnectionSocketFactory.TLS);
            context.init(null, null, null);
            factory = new SSLConnectionSocketFactory(context, NoopHostnameVerifier.INSTANCE);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("SSL上下文创建失败: " + e.getLocalizedMessage());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return factory;
    }

    private static String handle(HttpRequestBase requestType) {
        HttpClientContext context = HttpClientContext.create();
        CloseableHttpResponse response = null;
        String content = "entity is null!";
        try {
            response = client.execute(requestType, context);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                content = EntityUtils.toString(entity, StandardCharsets.UTF_8);
                EntityUtils.consume(entity);
            }
        } catch (ConnectTimeoutException cte) {
            System.out.println("请求连接超时，由于 " + cte.getLocalizedMessage());
            cte.printStackTrace();
        } catch (SocketTimeoutException ste) {
            System.out.println("请求通信超时，由于 " + ste.getLocalizedMessage());
            ste.printStackTrace();
        } catch (ClientProtocolException cpe) {
            System.out.println("协议错误（比如构造HttpGet对象时传入协议不对(将'http'写成'htp')or响应内容不符合），由于 " + cpe.getLocalizedMessage());
            cpe.printStackTrace();
        } catch (IOException e) {
            System.out.println("实体转换异常或者网络异常: " + e.getLocalizedMessage());
            e.printStackTrace();
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    System.out.println("response关闭异常: " + e.getLocalizedMessage());
                }
            }
            if (requestType != null) {
                requestType.releaseConnection();
            }
        }
        return content;
    }

    /**
     * 处理 get 请求
     *
     * @param url 请求地址
     * @return 响应体字符串
     */
    public static String get(String url) {
        HttpGet get = new HttpGet(url);
        return handle(get);
    }

    /**
     * 携带自定义cookie 发起get 请求
     *
     * @param url    请求url
     * @param cookie 自定义 cookie
     * @return 响应体字符串
     */
    public static String get(String url, String cookie) {
        HttpGet get = new HttpGet(url);
        if (StringUtils.isNotBlank(cookie)) {
            get.addHeader("cookie", cookie);
        }
        return handle(get);
    }

    /**
     * 发起get 请求
     *
     * @param url 请求url
     * @return 响应体字符串的字节数组
     */
    public static byte[] getAsByte(String url) {
        return get(url).getBytes();
    }

    public static String getHeaders(String url, String cookie, String headerName) {
        HttpGet get = new HttpGet(url);
        if (StringUtils.isNotBlank(cookie)) {
            get.addHeader("cookie", cookie);
        }
        handle(get);
        Header[] headers = get.getHeaders(headerName);
        return headers == null ? null : Arrays.toString(headers);
    }

    /**
     * get请求并附加用户信息
     */
    public static String getQQMusicWithRealHeader(String url) {
        HttpGet get = new HttpGet(url);
        get.addHeader(":authority", "y.qq.com");
        get.addHeader(":method", "GET");
        get.addHeader(":path", "/favicon.ico");
        get.addHeader(":schema", "https");
        get.addHeader("accept", "image/webp,image/apng,image/*,*/*;q=0.8");
        get.addHeader("accept-encoding", "gzip, deflate, br");
        get.addHeader("accept-language", "zh-CN,zh;q=0.9,en;q=0.8");
        get.addHeader("cache-control", "no-cache");
        get.addHeader("cookie", "yqq_stat=0");
        get.addHeader("pragma", "no-cache");
        get.addHeader("referer", "https://y.gtimg.cn/mediastyle/yqq/layout.css?max_age=25920000&v=20180803");
        get.addHeader("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36");
        get.addHeader("Referrer Policy", "no-referrer-when-downgrade");
        return handle(get);
    }

    public static String getNeteaseMusicWithRealHeader(String url) {
        HttpGet get = new HttpGet(url);
        get.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36");
        return handle(get);
    }



    public static String post(String url, String data) {
        HttpPost post = new HttpPost(url);
        if (StringUtils.isNotBlank(data)) {
            post.addHeader("Content-Type", "application/json");
        }
        post.setEntity(new StringEntity(data, ContentType.APPLICATION_JSON));
        return handle(post);
    }

    public static String post(String url, Map<String, String> headers, String cookie) {
        HttpPost post = new HttpPost(url);
        StringBuilder reqEntity = new StringBuilder();
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            post.addHeader(entry.getKey(), entry.getValue());
            try {
                reqEntity.append(entry.getKey())
                        .append("=")
                        .append(URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8.name()))
                        .append("&");
            } catch (UnsupportedEncodingException e) {
                System.out.println("请求实体转换异常，不支持的字符集，由于 " + e.getLocalizedMessage());
                e.printStackTrace();
            }
        }
        try {
            post.setEntity(new StringEntity(reqEntity.toString()));
        } catch (UnsupportedEncodingException e) {
            System.out.println("请求设置实体异常，不支持的字符集， 由于 " + e.getLocalizedMessage());
            e.printStackTrace();
        }

        if (StringUtils.isNotEmpty(cookie)) {
            post.addHeader("cookie", cookie);
        }
        return handle(post);
    }

}
