package com.turing.jira.singer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.turing.util.HttpUtil;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * @author yuwen
 * @date 2018/11/12
 */
public class SingerNameHandler {
    private int sin = 553440;
    private String urlStr = "https://u.y.qq.com/cgi-bin/musicu.fcg?callback=getUCGI09013026020383563&g_tk=5381&jsonpCallback=getUCGI09013026020383563&loginUin=0&hostUin=0&format=jsonp&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq&needNewCode=0&data=%7B%22comm%22%3A%7B%22ct%22%3A24%2C%22cv%22%3A10000%7D%2C%22singerList%22%3A%7B%22module%22%3A%22Music.SingerListServer%22%2C%22method%22%3A%22get_singer_list%22%2C%22param%22%3A%7B%22area%22%3A-100%2C%22sex%22%3A-100%2C%22genre%22%3A-100%2C%22index%22%3A-100%2C%22sin%22%3A"+sin+"%2C%22cur_page%22%3A1%7D%7D%7D";
    public void getSingerNameOnQQMusic() {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode singerList;
        String date, json;
        int count = 0;
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:\\chromedownload\\singerName2.txt", true)))){
            do {
                date = HttpUtil.getQQMusicWithRealHeader(urlStr);
                json = StringUtils.substring(date, StringUtils.indexOf(date, '(') + 1, StringUtils.lastIndexOf(date, ")"));
                JsonNode node = mapper.readTree(json);
                singerList = node.path("singerList").path("data").path("singerlist");
                if (singerList.size() == 0) {
                    while (count <= 20 && singerList.size() == 0) {
                        date = HttpUtil.getQQMusicWithRealHeader(urlStr);
                        json = StringUtils.substring(date, StringUtils.indexOf(date, '(') + 1, StringUtils.lastIndexOf(date, ")"));
                        node = mapper.readTree(json);
                        singerList = node.path("singerList").path("data").path("singerlist");
                        count ++;
                    }
                }
                for (JsonNode jsonNode : singerList) {
                    String singerName = jsonNode.path("singer_name").asText();
                    writer.write(singerName);
                    writer.newLine();
                }
                sin += 80;
                urlStr = "https://u.y.qq.com/cgi-bin/musicu.fcg?callback=getUCGI09013026020383563&g_tk=5381&jsonpCallback=getUCGI09013026020383563&loginUin=0&hostUin=0&format=jsonp&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq&needNewCode=0&data=%7B%22comm%22%3A%7B%22ct%22%3A24%2C%22cv%22%3A10000%7D%2C%22singerList%22%3A%7B%22module%22%3A%22Music.SingerListServer%22%2C%22method%22%3A%22get_singer_list%22%2C%22param%22%3A%7B%22area%22%3A-100%2C%22sex%22%3A-100%2C%22genre%22%3A-100%2C%22index%22%3A-100%2C%22sin%22%3A"+sin+"%2C%22cur_page%22%3A1%7D%7D%7D";
            } while (singerList.size() != 0);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
