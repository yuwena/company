package com.turing.task.singerandsong;

import com.turing.util.EmailUtil;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @author yuwen
 * @date 2018/11/14
 */
public class Main {

    public static void main(String[] args) throws SchedulerException {
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        Scheduler scheduler = schedulerFactory.getScheduler();
        scheduler.start();
        JobDetail jobDetail = JobBuilder.newJob(MainTask.class).withIdentity("singerAndSong", "reptile").build();
        CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                .withIdentity("singerAndSongTrigger", "reptileTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 0 ? * FRI"))
                .build();
        scheduler.scheduleJob(jobDetail, cronTrigger);

    }
}
