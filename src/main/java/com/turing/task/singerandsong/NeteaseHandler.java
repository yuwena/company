package com.turing.task.singerandsong;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.turing.util.HttpUtil;
import org.jsoup.Jsoup;
import org.jsoup.select.Selector;

import java.io.IOException;

/**
 * @author yuwen
 * @date 2018/11/13
 */
public class NeteaseHandler {

    private static String urlStr = "https://music.163.com/discover/toplist?id=3779629";

    /**
     * 获取 StringBuilder数组 包含歌手名和歌名  "@@@"分割
     * @return
     */
    public StringBuilder[] handleNeteaseNewMusic() {
        String html = HttpUtil.getNeteaseMusicWithRealHeader(urlStr);
        String json = getNeteaseJson(html);
        return getNeteaseSingerAndSong(json);
    }

    /**
     * 从返回的 html网页中 解析出目标json
     * @param html 网页接口返回 html
     * @return html中有效json
     */
    private  String getNeteaseJson(String html) {
        String json = null;
        try {
            json = Jsoup.parse(html).body()
                    .select("div#toplist")
                    .select("div.g-mn3")
                    .select("div.g-mn3c")
                    .select("div.g-wrap12")
                    .select("div#song-list-pre-cache")
                    .select("textarea#song-list-pre-data")
                    .text();
        } catch (Selector.SelectorParseException e) {
            throw new RuntimeException("网易云新歌部分, 接口html返回格式可能发生变化, 解析html失败, 位置: " + NeteaseHandler.class.getName());
        }
        return json;
    }

    /**
     * json 中循环解析出 歌手名以及新歌歌名
     * @param json json
     * @return StringBuilder数组(length == 2), 第 0 个元素包含歌手名, 第 1 个元素包含歌名, 以 "," 分割
     */
    private  StringBuilder[] getNeteaseSingerAndSong(String json) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode;
        StringBuilder songNameBuilder = new StringBuilder();
        StringBuilder singerNameBuilder = new StringBuilder();
        try {
            jsonNode = mapper.readTree(json);
            for (JsonNode node : jsonNode) {
                songNameBuilder.append(node.path("name")).append("@@@");
                JsonNode artists = node.path("artists");
                for (JsonNode artist : artists) {
                    singerNameBuilder.append(artist.path("name")).append("@@@");
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("网易云新歌 html解析json后, 进一步解析json发生错误, 位置: " + NeteaseHandler.class.getName());
        }
        return new StringBuilder[]{singerNameBuilder, songNameBuilder};
    }
}
