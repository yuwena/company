package com.turing.task.singerandsong;

import com.turing.util.EmailUtil;
import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author yuwen
 * @date 2018/11/13
 */
public class MainTask implements Job {


    /**
     * 去掉字符串两侧双引号
     *
     * @param s
     * @return
     */
    private static String cutString(String s) {
        if (StringUtils.startsWith(s, "\"") && StringUtils.endsWith(s, "\"")) {
            s = StringUtils.substring(s, 1, s.length() -1);
        }
        return s;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        System.out.println("执行");
        QQHandler qqHandler = new QQHandler();
        NeteaseHandler neteaseHandler = new NeteaseHandler();
        XiaMiHandler xiaMiHandler = new XiaMiHandler();
        StringBuilder[] qqBuilder = null, neteaseBuilder = null, xiaMiBuilder = null;
        try {
            qqBuilder = qqHandler.handleQQNewMusic();
            neteaseBuilder = neteaseHandler.handleNeteaseNewMusic();
            xiaMiBuilder = xiaMiHandler.handleXiaMiNewMusic();
        } catch (Exception e) {
            new EmailUtil("yuwenzhicheng@uzoo.cn", "Xnn17h123@yyh")
                    .setMyNickName("yuwenzhicheng")
                    .addRecipientT0("yuwenzhicheng@uzoo.cn")
                    .createMail("每周新歌", "MainTask中出现异常, 三个网站接口可能发生变化" + e.getMessage(), "text/html;charset=utf-8")
                    .sendEmail(null);
            System.exit(0);
        }
        String singerName = qqBuilder[0].append(neteaseBuilder[0]).append(xiaMiBuilder[0]).toString();
        String songName = qqBuilder[1].append(neteaseBuilder[1]).append(xiaMiBuilder[1]).toString();

        Set<String> songNameSet = new HashSet<>(Arrays.asList(songName.split("@@@")));
        Set<String> singerSet = new HashSet<>(Arrays.asList(singerName.split("@@@")));

        try (BufferedWriter writer1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("song.txt")));
             BufferedWriter writer2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("singer.txt")))) {
            for (String s : songNameSet) {
                s = cutString(s);
                writer1.write(s);
                writer1.newLine();
            }
            for (String s : singerSet) {
                s = cutString(s);
                writer2.write(s);
                writer2.newLine();
            }
            writer1.flush();
            writer2.flush();
            new EmailUtil("yuwenzhicheng@uzoo.cn", "Xnn17h123@yyh")
                    .setMyNickName("yuwenzhicheng")
                    .addFile("song.txt")
                    .addFile("singer.txt")
                    .setSaveEmail("email.eml")
                    .addRecipientT0("manhaoyang@uzoo.cn")
                    .addRecipientT0("guowenhua@uzoo.cn")
                    .addRecipientT0("wangconghui@uzoo.cn")
                    .addRecipientT0("anqi@uzoo.cn")
                    .addRecipientT0("kuangyaming@uzoo.cn")
                    .createMail("每周新歌", "歌名附件: song.txt\n 歌手附件: singer.txt", "text/html;charset=utf-8")
                    .sendEmail(null);
        } catch (IOException e) {
            new EmailUtil("yuwenzhicheng@uzoo.cn", "Xnn17h123@yyh")
                    .setMyNickName("yuwenzhicheng")
                    .addRecipientT0("yuwenzhicheng@uzoo.cn")
                    .createMail("每周新歌", "MainTask中出现异常", "text/html;charset=utf-8")
                    .sendEmail(null);
        }

    }

    public void run() {
        System.out.println("执行");
        QQHandler qqHandler = new QQHandler();
        NeteaseHandler neteaseHandler = new NeteaseHandler();
        XiaMiHandler xiaMiHandler = new XiaMiHandler();
        StringBuilder[] qqBuilder = null, neteaseBuilder = null, xiaMiBuilder = null;
        try {
            qqBuilder = qqHandler.handleQQNewMusic();
            neteaseBuilder = neteaseHandler.handleNeteaseNewMusic();
            xiaMiBuilder = xiaMiHandler.handleXiaMiNewMusic();
        } catch (Exception e) {
            new EmailUtil("yuwenzhicheng@uzoo.cn", "Xnn17h123@yyh")
                    .setMyNickName("yuwenzhicheng")
                    .addRecipientT0("yuwenzhicheng@uzoo.cn")
                    .createMail("每周新歌", "MainTask中出现异常, 三个网站接口可能发生变化" + e.getMessage(), "text/html;charset=utf-8")
                    .sendEmail(null);
            System.exit(0);
        }
        String singerName = qqBuilder[0].append(neteaseBuilder[0]).append(xiaMiBuilder[0]).toString();
        String songName = qqBuilder[1].append(neteaseBuilder[1]).append(xiaMiBuilder[1]).toString();

        Set<String> songNameSet = new HashSet<>(Arrays.asList(songName.split("@@@")));
        Set<String> singerSet = new HashSet<>(Arrays.asList(singerName.split("@@@")));

        try (BufferedWriter writer1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("song.txt")));
             BufferedWriter writer2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("singer.txt")))) {
            for (String s : songNameSet) {
                s = cutString(s);
                writer1.write(s);
                writer1.newLine();
            }
            for (String s : singerSet) {
                s = cutString(s);
                writer2.write(s);
                writer2.newLine();
            }
            writer1.flush();
            writer2.flush();
            new EmailUtil("yuwenzhicheng@uzoo.cn", "Xnn17h123@yyh")
                    .setMyNickName("yuwenzhicheng")
                    .addFile("song.txt")
                    .addFile("singer.txt")
                    .setSaveEmail("email.eml")
                    .addRecipientT0("manhaoyang@uzoo.cn")
                    .addRecipientT0("guowenhua@uzoo.cn")
                    .addRecipientT0("wangconghui@uzoo.cn")
                    .addRecipientT0("anqi@uzoo.cn")
                    .addRecipientT0("kuangyaming@uzoo.cn")
                    .addRecipientT0("yuwenzhicheng@uzoo.cn")
                    .createMail("每周新歌", "歌名附件: song.txt\n 歌手附件: singer.txt", "text/html;charset=utf-8")
                    .sendEmail(null);
        } catch (IOException e) {
            new EmailUtil("yuwenzhicheng@uzoo.cn", "Xnn17h123@yyh")
                    .setMyNickName("yuwenzhicheng")
                    .addRecipientT0("yuwenzhicheng@uzoo.cn")
                    .createMail("每周新歌", "MainTask中出现异常", "text/html;charset=utf-8")
                    .sendEmail(null);
        }
    }


    public static void main(String[] args) {
        new MainTask().run();
    }
}
