package com.turing.task.singerandsong;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.turing.util.HttpUtil;
import com.turing.util.JsonUtil;
import com.turing.util.URLUtil;

import java.io.IOException;

/**
 * @author yuwen
 * @date 2018/11/13
 */
public class QQHandler {

    /**
     * qq音乐新歌接口 ,  返回json
     */
    private static String urlStr = "https://u.y.qq.com/cgi-bin/musicu.fcg?callback=recom836311920427709&g_tk=5381&jsonpCallback=recom836311920427709&loginUin=0&hostUin=0&format=jsonp&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq&needNewCode=0&data={\"comm\":{\"ct\":24},\"category\":{\"method\":\"get_hot_category\",\"param\":{\"qq\":\"\"},\"module\":\"music.web_category_svr\"},\"recomPlaylist\":{\"method\":\"get_hot_recommend\",\"param\":{\"async\":1,\"cmd\":2},\"module\":\"playlist.HotRecommendServer\"},\"playlist\":{\"method\":\"get_playlist_by_category\",\"param\":{\"id\":8,\"curPage\":1,\"size\":40,\"order\":5,\"titleid\":8},\"module\":\"playlist.PlayListPlazaServer\"},\"new_song\":{\"module\":\"QQMusic.MusichallServer\",\"method\":\"GetNewSong\",\"param\":{\"type\":0}},\"new_album\":{\"module\":\"music.web_album_library\",\"method\":\"get_album_by_tags\",\"param\":{\"area\":1,\"company\":-1,\"genre\":-1,\"type\":-1,\"year\":-1,\"sort\":2,\"get_tags\":1,\"sin\":0,\"num\":40,\"click_albumid\":0}},\"toplist\":{\"module\":\"music.web_toplist_svr\",\"method\":\"get_toplist_index\",\"param\":{}},\"focus\":{\"module\":\"QQMusic.MusichallServer\",\"method\":\"GetFocus\",\"param\":{}}}";

    /**
     * 处理 urlStr 链接
     * @return StringBuilder数组(length == 2)   数组第 0 个元素包含歌手名, 数组第 1 个元素包含歌名， 以'@@@'分割
     *         如果返回值为空  则可能接口返回的json有问题  line: 44
     */
    public StringBuilder[] handleQQNewMusic() {
        final String url = URLUtil.handleQQMusicUrl(urlStr);
        StringBuilder songNameBuilder = new StringBuilder();
        StringBuilder singerNameBuilder = new StringBuilder();
        if (url != null) {
            String json = HttpUtil.get(url);
            json = JsonUtil.handleQQMusicJson(json);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode;
            try {
                jsonNode = mapper.readTree(json);
            } catch (IOException e) {
                throw new RuntimeException("QQ音乐新歌部分, 获取接口返回json, 解析发生错误, 位置: " + QQHandler.class.getName());
            }
            if (jsonNode.path("code").asInt() == 0) {
                jsonNode = jsonNode.path("new_song");
                if (jsonNode.path("code").asInt() == 0) {
                    jsonNode = jsonNode.path("data").path("song_list");
                    for (JsonNode node : jsonNode) {
                        songNameBuilder.append(node.path("name").asText()).append("@@@");
                        for (JsonNode singer : node.path("singer")) {
                            singerNameBuilder.append(singer.path("name")).append("@@@");
                        }
                    }
                }
            }
        }
        return new StringBuilder[]{singerNameBuilder, songNameBuilder};
    }
}
