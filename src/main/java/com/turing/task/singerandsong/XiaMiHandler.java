package com.turing.task.singerandsong;

import com.turing.util.HttpUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author yuwen
 * @date 2018/11/13
 */
public class XiaMiHandler {

    private static final String urlStr = "https://www.xiami.com/chart/data?c=102&type=0&page=1&limit=100";

    private void handleException(Elements elements) {
        if (elements.size() == 0) {
            throw new RuntimeException("虾米新歌曲部分, 接口返回html解析失败, 可能已经发生变化, 位置: " + XiaMiHandler.class.getName());
        }
    }

    /**
     * 虾米音乐新歌, 以及歌手名
     * @return StringBuilder数组 (length == 2), 第 0 个元素为歌手名, 第 1 个元素为歌名,  以 "@@@"分割
     */
    public StringBuilder[] handleXiaMiNewMusic() {
        String html = HttpUtil.get(urlStr);
        Elements elements = Jsoup.parse(html).body().select("div.song");
        handleException(elements);
        StringBuilder singerNameBuilder = new StringBuilder();
        StringBuilder songNameBuilder = new StringBuilder();
        for (Element element : elements) {
            Elements select = element.select("div.info").select("p");
            handleException(select);
            for (Element element1 : select) {
                Elements strong = element1.select("strong");
                if (strong.size() != 0) {
                    songNameBuilder.append(strong.select("a").text()).append("@@@");
                } else {
                    singerNameBuilder.append(element1.select("a").text()).append("@@@");
                }
            }
        }
        return new StringBuilder[]{singerNameBuilder, songNameBuilder};
    }
}